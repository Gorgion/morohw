import React, {Component} from "react";
import TableCell from "./TableCell";
import TableCellButton from "./TableCellButton"
import PropTypes from 'prop-types';

class TableRow extends Component {
    render() {
        return (
            <tr>
                {Object.keys(this.props.person).map((key, i) => <TableCell cell={this.props.person[key]} key={i} />)}
                <TableCellButton onRowDeleted={this.props.onRowDeleted} person={this.props.person} key={this.props.person.id} />
            </tr>
        );
    }
}

TableRow.propTypes = {
    person: PropTypes.object
};

export default TableRow;
