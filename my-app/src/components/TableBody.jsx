import React, {Component} from "react";
import TableRow from "./TableRow";
import PropTypes from 'prop-types';

class TableBody extends Component {
    render() {
        return (
            <tbody>
                {this.props.rows.map((person, i) => <TableRow key={person.id} person={person} onRowDeleted={this.props.onRowDeleted} />)}
            </tbody>
        );
    }
}

TableBody.propTypes = {
    rows: PropTypes.array
};

export default TableBody;
