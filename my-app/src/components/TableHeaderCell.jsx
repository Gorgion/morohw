import React, {Component} from "react";
import PropTypes from "prop-types";

class TableHeaderCell extends Component {
    render() {
        return (
            <th>{this.props.cell}</th>
        );
    }
}

TableHeaderCell.propTypes = {
    cell: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};

export default TableHeaderCell;
