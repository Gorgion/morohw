import React, {Component} from "react";
import PropTypes from "prop-types";
import TableBody from "./TableBody";
import TableHeader from "./TableHeader";
import AddTextField from "./AddTextField";

class Table extends Component {

    render() {
        return (
            <div>
                <table>
                    <TableHeader header={this.props.header}/>
                    <TableBody rows={this.props.rows} onRowDeleted={this.props.onRowDeleted}/>
                </table>
                <AddTextField onRowAdd={this.props.onRowAdd} />
            </div>
        )
    }
}

Table.propTypes = {
    header: PropTypes.array,
    rows: PropTypes.array,
    onRowAdd: PropTypes.callback,
    onRowDeleted: PropTypes.callback
};

export default Table;
