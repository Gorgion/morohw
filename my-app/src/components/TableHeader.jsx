import React, {Component} from "react";
import TableHeaderRow from "./TableHeaderRow"
import PropTypes from 'prop-types';

class TableHeader extends Component {
    render() {
        return (
            <thead>
                <TableHeaderRow row={this.props.header} />
            </thead>
        );
    }
}

TableHeader.propTypes = {
    header: PropTypes.array
};

export default TableHeader;
