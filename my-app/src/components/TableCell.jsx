import React, {Component} from "react";
import PropTypes from "prop-types";

class TableCell extends Component {
    render() {
        return (
            <td>{this.props.cell}</td>
        );
    }
}

TableCell.propTypes = {
    cell: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};

export default TableCell;
