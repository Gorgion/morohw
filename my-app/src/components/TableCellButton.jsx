import React, {Component} from "react";
import PropTypes from "prop-types";

class TableCellButton extends Component {
    onRowDeleted() {
        this.props.onRowDeleted(this.props.person);
    }

    render() {
        return (
            <td><button type="button" onClick={this.onRowDeleted.bind(this)} >Smazat</button></td>
        );
    }
}

TableCellButton.propTypes = {
    // removeRow: PropTypes.callback
};

export default TableCellButton;
