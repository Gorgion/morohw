import React, {Component} from "react";
import PropTypes from "prop-types";

class AddTextField extends Component {
    constructor(props) {
        super(props);

        this.state = {
            person: {
                id: '',
                name: ''
            }
        }

        this.handleRowAdd = this.handleRowAdd.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
    }

    handleRowAdd() {
        this.props.onRowAdd(this.state.person);
    }
    handleNameChange(e) {
        this.state.person.name = e.target.value;
    }

    render() {
        return (
            <div>
                <input name="name" onChange={this.handleNameChange}/>
                <button onClick={this.handleRowAdd}>Přidat</button>
            </div>
        );
    }
}

AddTextField.propTypes = {
    onRowAdd: PropTypes.callback
};

export default AddTextField;
