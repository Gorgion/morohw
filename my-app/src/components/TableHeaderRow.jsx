import React, {Component} from "react";
import TableHeaderCell from "./TableHeaderCell";
import PropTypes from 'prop-types';

class TableHeaderRow extends Component {
    render() {
        return (
            <tr>
                {Object.keys(this.props.row).map((key) => <TableHeaderCell cell={this.props.row[key]} key={key} />)}
            </tr>
        );
    }
}

TableHeaderRow.propTypes = {
    row: PropTypes.array
};

export default TableHeaderRow;
