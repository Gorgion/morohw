import React, {Component} from "react";
import Table from "./components/Table";
import "./App.css";

class App extends Component {
    constructor() {
        super();

        this.state = {
            header: [
                "ID",
                "Name"
            ],
            rows: [
                {
                    "id": 1,
                    "name": "Name1"
                },

                {
                    "id": 2,
                    "name": "Name2"
                },

                {
                    "id": 3,
                    "name": "Name3"
                }
            ]
        };

        this.handleRowDeleted = this.handleRowDeleted.bind(this);
        this.handleRowAdd = this.handleRowAdd.bind(this);
    };

    handleRowAdd(person) {
        var id = 1;
        if (this.state.rows.length > 0) {
            id = this.state.rows[this.state.rows.length - 1].id + 1;
        }
console.log(this.state.rows);
        // person.id = id;
        var newPerson = {
            "id": id,
            "name": person.name
        };

        this.state.rows.push(newPerson);
        this.setState(this.state.rows);
    }

    handleRowDeleted(person) {
        // this.setState({data: e.target.value});
        // console.log(person);
        var index = this.state.rows.indexOf(person);

        this.state.rows.splice(index, 1);
        this.setState(this.state);
    };

    render() {
        return (
            <div>
                <Table header={this.state.header} rows={this.state.rows}
                       onRowDeleted={this.handleRowDeleted}
                       onRowAdd={this.handleRowAdd}/>
            </div>
        );
    }
}

export default App;
